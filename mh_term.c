/*
 * mh_term.c
 *
 *  Created on: 1 Dec 2020
 *      Author: tristan
 */

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

typedef uint32_t mh_bounds_t;

typedef enum {
	KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9,

	KEY_KEYPAD_0, KEY_KEYPAD_00, KEY_KEYPAD_000, KEY_KEYPAD_1, KEY_KEYPAD_2, KEY_KEYPAD_3, KEY_KEYPAD_4, KEY_KEYPAD_5, KEY_KEYPAD_6, KEY_KEYPAD_7,
	KEY_KEYPAD_8, KEY_KEYPAD_9, KEY_KEYPAD_A, KEY_KEYPAD_B, KEY_KEYPAD_C, KEY_KEYPAD_D, KEY_KEYPAD_E, KEY_KEYPAD_F, KEY_KEYPAD_AMPERSAND, KEY_KEYPAD_AT,
	KEY_KEYPAD_BACKSPACE, KEY_KEYPAD_BINARY, KEY_KEYPAD_CLEAR, KEY_KEYPAD_CLEAR_ENTRY, KEY_KEYPAD_COLON, KEY_KEYPAD_COMMA, KEY_KEYPAD_DOUBLE_AMPERSAND,
	KEY_KEYPAD_DOUBLE_BAR, KEY_KEYPAD_DECIMAL, KEY_KEYPAD_SLASH, KEY_KEYPAD_ENTER, KEY_KEYPAD_EQUALS, KEY_KEYPAD_EQUALS_AS400, KEY_KEYPAD_EXCLAMATION,
	KEY_KEYPAD_GREATER_THAN, KEY_KEYPAD_HASH, KEY_KEYPAD_HEXADECIMAL, KEY_KEYPAD_LEFT_BRACE, KEY_KEYPAD_LEFT_PARENTHESIS, KEY_KEYPAD_LESS_THAN, KEY_KEYPAD_MEM_ADD,
	KEY_KEYPAD_MEM_CLEAR, KEY_KEYPAD_MEM_DIVIDE, KEY_KEYPAD_MEM_MULTIPLY, KEY_KEYPAD_MEM_RECALL, KEY_KEYPAD_MEM_STORE, KEY_KEYPAD_MEM_SUBTRACT, KEY_KEYPAD_MINUS,
	KEY_KEYPAD_ASTERISK, KEY_KEYPAD_OCTAL, KEY_KEYPAD_PERCENT, KEY_KEYPAD_PERIOD, KEY_KEYPAD_PLUS, KEY_KEYPAD_PLUS_MINUS, KEY_KEYPAD_CARET, KEY_KEYPAD_RIGHT_BRACE,
	KEY_KEYPAD_RIGHT_PARENTHESIS, KEY_KEYPAD_SPACE, KEY_KEYPAD_BAR, KEY_KEYPAD_XOR, KEY_KEYPAD_TAB,

	KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J, KEY_K, KEY_L, KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q,
	KEY_R, KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z,

	KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_UP,

	KEY_APOSTROPHE, KEY_BACKSLASH, KEY_COMMA, KEY_EQUALS, KEY_GRAVE, KEY_LEFT_BRACKET, KEY_MINUS, KEY_PERIOD, KEY_RIGHT_BRACKET, KEY_SEMICOLON,
	KEY_SLASH,

	KEY_BACKSPACE, KEY_DELETE, KEY_END, KEY_ESCAPE, KEY_HOME, KEY_INSERT, KEY_LEFT_ALT, KEY_LEFT_CTRL, KEY_LEFT_META, KEY_LEFT_SHIFT, KEY_PAGE_DOWN,
	KEY_PAGE_UP, KEY_PAUSE, KEY_PRINT_SCREEN, KEY_RIGHT_ALT, KEY_RIGHT_CTRL, KEY_ENTER, KEY_RETURN, KEY_RIGHT_META, KEY_RIGHT_SHIFT, KEY_SPACE,
	KEY_SYS_REQ, KEY_TAB,

	KEY_CAPSLOCK, KEY_NUMLOCK, KEY_SCROLLLOCK,

	KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12, KEY_F13, KEY_F14, KEY_F15,
	KEY_F16, KEY_F17, KEY_F18, KEY_F19, KEY_F20, KEY_F21, KEY_F22, KEY_F23, KEY_F24,

	KEY_AC_BACK, KEY_AC_BOOKMARKS, KEY_AC_FORWARD, KEY_AC_HOME, KEY_AC_REFRESH, KEY_AC_SEARCH, KEY_AC_STOP, KEY_AGAIN, KEY_ALT_ERASE, KEY_APPLICATION,
	KEY_AUDIO_FAST_FORWARD, KEY_AUDIO_MUTE, KEY_AUDIO_NEXT, KEY_AUDIO_PLAY, KEY_AUDIO_PREVIOUS, KEY_AUDIO_REWIND, KEY_AUDIO_STOP, KEY_BRIGHTNESS_DOWN,
	KEY_BRIGHTNESS_UP, KEY_CALCULATOR, KEY_CANCEL, KEY_CLEAR, KEY_CLEAR_AGAIN, KEY_MY_COMPUTER, KEY_COPY, KEY_CRSEL, KEY_CURRENCY_SUB_UNIT,
	KEY_CURRENCY_UNIT, KEY_CUT, KEY_DECIMAL_SEPARATOR, KEY_DISPLAY_SWITCH, KEY_EJECT, KEY_EXECUTE, KEY_EXSEL, KEY_FIND, KEY_HELP,
	KEY_KEYBOARD_ILLUMINATION_DOWN, KEY_KEYBOARD_ILLUMINATION_TOGGLE, KEY_KEYBOARD_ILLUMINATION_UP, KEY_MAIL, KEY_MEDIA_SELECT, KEY_MENU, KEY_MODE_SWITCH, KEY_MUTE,
	KEY_OPER, KEY_OUT, KEY_PASTE, KEY_POWER, KEY_PRIOR, KEY_SELECT, KEY_SEPARATOR, KEY_SLEEP, KEY_STOP, KEY_THOUSANDS_SEPARATOR, KEY_UNDO,
	KEY_VOLUME_DOWN, KEY_VOLUME_UP, KEY_WWW, KEY_APP_1, KEY_APP_2,

	KEY_LAST_KEY
} _mh_key_t;

#define KEY_FIRST_KEY	KEY_0
#define KEY_UNKNOWN		KEY_LAST_KEY

typedef enum {
	STATE_NONE
} _mh_state_t;

typedef struct {
	uint8_t x, y;
} _mh_glyph_t;

typedef struct {
	uint8_t r, g, b;
} _mh_color_t;

typedef struct {
	uint8_t glyph;
	_mh_color_t color;
} _mh_character_t;

typedef struct {
	char *title;
	SDL_Window *window;
	SDL_Renderer *renderer;
	uint8_t width, height, glyph_width, glyph_height;
	SDL_Texture *font;
	uint16_t font_width, font_height;
	_mh_glyph_t *glyph;
	_mh_character_t *screen;
	_mh_state_t state;
	bool shutdown;
	bool keys_down[KEY_LAST_KEY];
} _mh_term_t;

static bool _sdl_init = false;
static bool _img_init = false;
static uint64_t _terminal_count = 0;

static _mh_key_t _sdl_scancode_to_mh_key_t[] = {
		KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J, KEY_K,
		KEY_L, KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z, KEY_1, KEY_2,
		KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0, KEY_ENTER, KEY_ESCAPE, KEY_BACKSPACE, KEY_TAB, KEY_SPACE, KEY_MINUS,
		KEY_EQUALS, KEY_LEFT_BRACKET, KEY_RIGHT_BRACKET, KEY_BACKSLASH, KEY_BACKSLASH, KEY_SEMICOLON, KEY_APOSTROPHE, KEY_GRAVE, KEY_COMMA, KEY_PERIOD,
		KEY_SLASH, KEY_CAPSLOCK, KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12,
		KEY_PRINT_SCREEN, KEY_SCROLLLOCK, KEY_PAUSE, KEY_INSERT, KEY_HOME, KEY_PAGE_UP, KEY_DELETE, KEY_END, KEY_PAGE_DOWN, KEY_RIGHT, KEY_LEFT,
		KEY_DOWN, KEY_UP, KEY_NUMLOCK, KEY_KEYPAD_SLASH, KEY_KEYPAD_ASTERISK, KEY_KEYPAD_MINUS, KEY_KEYPAD_PLUS, KEY_KEYPAD_ENTER, KEY_KEYPAD_1,
		KEY_KEYPAD_2, KEY_KEYPAD_3, KEY_KEYPAD_4, KEY_KEYPAD_5, KEY_KEYPAD_6, KEY_KEYPAD_7, KEY_KEYPAD_8, KEY_KEYPAD_9, KEY_KEYPAD_0, KEY_KEYPAD_PERIOD,
		KEY_BACKSLASH, KEY_APPLICATION, KEY_POWER, KEY_KEYPAD_EQUALS, KEY_F13, KEY_F14, KEY_F15, KEY_F16, KEY_F17, KEY_F18, KEY_F19, KEY_F20,
		KEY_F21, KEY_F22, KEY_F23, KEY_F24, KEY_EXECUTE, KEY_HELP, KEY_MENU, KEY_SELECT, KEY_STOP, KEY_AGAIN, KEY_UNDO, KEY_CUT, KEY_COPY,
		KEY_PASTE, KEY_FIND, KEY_MUTE, KEY_VOLUME_UP, KEY_VOLUME_DOWN, KEY_KEYPAD_COMMA, KEY_KEYPAD_EQUALS_AS400, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,
		KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,
		KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_ALT_ERASE, KEY_SYS_REQ, KEY_CANCEL, KEY_CLEAR, KEY_PRIOR, KEY_RETURN, KEY_SEPARATOR,
		KEY_OUT, KEY_OPER, KEY_CLEAR_AGAIN, KEY_CRSEL, KEY_EXSEL, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,
		KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_KEYPAD_00, KEY_KEYPAD_000, KEY_THOUSANDS_SEPARATOR, KEY_DECIMAL_SEPARATOR, KEY_CURRENCY_UNIT,
		KEY_CURRENCY_SUB_UNIT, KEY_KEYPAD_LEFT_PARENTHESIS, KEY_KEYPAD_RIGHT_PARENTHESIS, KEY_KEYPAD_LEFT_BRACE, KEY_KEYPAD_RIGHT_BRACE, KEY_KEYPAD_TAB,
		KEY_KEYPAD_BACKSPACE, KEY_KEYPAD_A, KEY_KEYPAD_B, KEY_KEYPAD_C, KEY_KEYPAD_D, KEY_KEYPAD_E, KEY_KEYPAD_F, KEY_KEYPAD_XOR, KEY_KEYPAD_CARET,
		KEY_KEYPAD_PERCENT, KEY_KEYPAD_LESS_THAN, KEY_KEYPAD_GREATER_THAN, KEY_KEYPAD_AMPERSAND, KEY_KEYPAD_DOUBLE_AMPERSAND, KEY_KEYPAD_BAR, KEY_KEYPAD_DOUBLE_BAR,
		KEY_KEYPAD_COLON, KEY_KEYPAD_HASH, KEY_KEYPAD_SPACE, KEY_KEYPAD_AT, KEY_KEYPAD_EXCLAMATION, KEY_KEYPAD_MEM_STORE, KEY_KEYPAD_MEM_RECALL, KEY_KEYPAD_MEM_CLEAR,
		KEY_KEYPAD_MEM_ADD, KEY_KEYPAD_MEM_SUBTRACT, KEY_KEYPAD_MEM_MULTIPLY, KEY_KEYPAD_MEM_DIVIDE, KEY_KEYPAD_PLUS_MINUS, KEY_KEYPAD_CLEAR, KEY_KEYPAD_CLEAR_ENTRY,
		KEY_KEYPAD_BINARY, KEY_KEYPAD_OCTAL, KEY_KEYPAD_DECIMAL, KEY_KEYPAD_HEXADECIMAL, KEY_UNKNOWN, KEY_UNKNOWN, KEY_LEFT_CTRL, KEY_LEFT_SHIFT, KEY_LEFT_ALT,
		KEY_LEFT_META, KEY_RIGHT_CTRL, KEY_RIGHT_SHIFT, KEY_RIGHT_ALT, KEY_RIGHT_META, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,
		KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,
		KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN, KEY_UNKNOWN,
		KEY_AUDIO_NEXT, KEY_AUDIO_PREVIOUS, KEY_AUDIO_STOP, KEY_AUDIO_PLAY, KEY_AUDIO_MUTE, KEY_MEDIA_SELECT, KEY_WWW, KEY_MAIL, KEY_CALCULATOR,
		KEY_MY_COMPUTER, KEY_AC_SEARCH, KEY_AC_HOME, KEY_AC_BACK, KEY_AC_FORWARD, KEY_AC_STOP, KEY_AC_REFRESH, KEY_AC_BOOKMARKS, KEY_BRIGHTNESS_DOWN,
		KEY_BRIGHTNESS_UP, KEY_DISPLAY_SWITCH, KEY_KEYBOARD_ILLUMINATION_TOGGLE, KEY_KEYBOARD_ILLUMINATION_DOWN, KEY_KEYBOARD_ILLUMINATION_UP, KEY_EJECT, KEY_SLEEP,
		KEY_APP_1, KEY_APP_2, KEY_AUDIO_REWIND, KEY_AUDIO_FAST_FORWARD
};

void terminal_destroy(_mh_term_t *terminal) {
	if (terminal == NULL) {
		return;
	}

	_terminal_count--;

	if (terminal->font != NULL) {
		SDL_DestroyTexture(terminal->font);
	}

	if (terminal->renderer != NULL) {
		SDL_DestroyRenderer(terminal->renderer);
	}

	if (terminal->window != NULL) {
		SDL_DestroyWindow(terminal->window);
	}

	if (_img_init && _terminal_count == 0) {
		IMG_Quit();
	}

	if (_sdl_init && _terminal_count == 0) {
		SDL_Quit();
	}

	if (terminal->glyph != NULL) {
		free(terminal->glyph);
	}

	if (terminal->screen != NULL) {
		free(terminal->screen);
	}

	if (terminal->title != NULL) {
		free(terminal->title);
	}

	if (terminal != NULL) {
		free(terminal);
	}
}

_mh_term_t *terminal_create(const char *title, uint8_t width, uint8_t height, const char *font_path, uint8_t glyph_width, uint8_t glyph_height) {
	_mh_term_t *terminal = (_mh_term_t*)malloc(sizeof(_mh_term_t));
	if (terminal == NULL) {
		fprintf(stderr, "Failed to create terminal.\n");

		return NULL;
	}

	terminal->title = NULL;
	terminal->window = NULL;
	terminal->renderer = NULL;
	terminal->width = 0;
	terminal->height = 0;
	terminal->glyph_width = 0;
	terminal->glyph_height = 0;
	terminal->font = NULL;
	terminal->font_width = 0;
	terminal->font_height = 0;
	terminal->glyph = NULL;
	terminal->screen = NULL;
	terminal->state = STATE_NONE;
	terminal->shutdown = false;

	for (int i = 0; i < KEY_LAST_KEY; i++) {
		terminal->keys_down[i] = false;
	}

	if (!_sdl_init) {
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			fprintf(stderr, "Failed to initialise SDL.\n%s\n", SDL_GetError());
			free(terminal);

			return NULL;
		}

		_sdl_init = true;
	}

	if (!_img_init) {
		if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
			fprintf(stderr, "Failed to initialise IMG.\n%s\n", IMG_GetError());
			terminal_destroy(terminal);

			return NULL;
		}

		_img_init = true;
	}

	terminal->window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width * glyph_width, height * glyph_height, 0);
	if (terminal->window == NULL) {
		fprintf(stderr, "Failed to create window.\n%s\n", SDL_GetError());
		terminal_destroy(terminal);

		return NULL;
	}

	terminal->renderer = SDL_CreateRenderer(terminal->window, -1, SDL_RENDERER_ACCELERATED);
	if (terminal->renderer == NULL) {
		fprintf(stderr, "Failed to create renderer.\n%s\n", SDL_GetError());
		terminal_destroy(terminal);

		return NULL;
	}

	terminal->title = (char*)malloc(sizeof(char) * strlen(title));
	if (terminal->title == NULL) {
		fprintf(stderr, "Failed to acquire memory for title.\n");
		terminal_destroy(terminal);
	}

	strcpy(terminal->title, title);

	terminal->width = width;
	terminal->height = height;
	terminal->glyph_width = glyph_width;
	terminal->glyph_height = glyph_height;

	SDL_Surface *font_surface = IMG_Load(font_path);
	if (!font_surface) {
		fprintf(stderr, "Failed to load font image file.\n%s\n", IMG_GetError());
		terminal_destroy(terminal);

		return NULL;
	}

	terminal->font = SDL_CreateTextureFromSurface(terminal->renderer, font_surface);

	SDL_FreeSurface(font_surface);

	int fw, fh;

	SDL_QueryTexture(terminal->font, NULL, NULL, &fw, &fh);

	terminal->font_width = (uint16_t)fw;
	terminal->font_height = (uint16_t)fh;

	uint8_t w = terminal->font_width / terminal->glyph_width;
	uint8_t h = terminal->font_height / terminal->glyph_height;

	terminal->glyph = (_mh_glyph_t*)malloc(sizeof(_mh_glyph_t) * w * h);
	if (terminal->glyph == NULL) {
		fprintf(stderr, "Failed to acquire memory for glyph table.\n");
		terminal_destroy(terminal);

		return NULL;
	}

	for (uint8_t x = 0; x < w; x++) {
		for (uint8_t y = 0; y < h; y++) {
			terminal->glyph[x + y * w].x = x * terminal->glyph_width;
			terminal->glyph[x + y * w].y = y * terminal->glyph_height;
		}
	}

	terminal->screen = (_mh_character_t*)malloc(sizeof(_mh_character_t) * terminal->width * terminal->height);
	if (terminal->screen == NULL) {
		fprintf(stderr, "Failed to acquire memory for screen data.\n");
		terminal_destroy(terminal);

		return NULL;
	}

	for (uint16_t i = 0; i < terminal->width * terminal->height; i++) {
		terminal->screen[i].glyph = 0;
		terminal->screen[i].color.r = 0xff;
		terminal->screen[i].color.g = 0xff;
		terminal->screen[i].color.b = 0xff;
	}

	_terminal_count++;

	return terminal;
}

void terminal_refresh(_mh_term_t *terminal) {
	for (uint8_t y = 0; y < terminal->height; y++) {
		for (uint8_t x = 0; x < terminal->width; x++) {
			_mh_character_t *ch = &terminal->screen[x + y * terminal->width];

			_mh_glyph_t *glyph = &terminal->glyph[ch->glyph];
			SDL_Rect src = { glyph->x, glyph->y, terminal->glyph_width, terminal->glyph_height };
			SDL_Rect dst = { x * terminal->glyph_width, y * terminal->glyph_height, terminal->glyph_width, terminal->glyph_height };

			SDL_SetTextureColorMod(terminal->font, ch->color.r, ch->color.g, ch->color.b);
			SDL_RenderCopy(terminal->renderer, terminal->font, &src, &dst);
			SDL_SetTextureColorMod(terminal->font, 0xff, 0xff, 0xff);
		}
	}

	SDL_RenderPresent(terminal->renderer);
}

bool terminal_should_close(_mh_term_t *terminal) {
	return terminal->shutdown;
}

void terminal_set_close(_mh_term_t *terminal, bool set) {
	terminal->shutdown = set;
}

void terminal_put_char(_mh_term_t *terminal, char ch, uint8_t x, uint8_t y) {
	if (x < 0 || x >= terminal->width || y < 0 || y >= terminal->height) {
		return;
	}

	terminal->screen[x + y * terminal->width].glyph = ch;
}

void terminal_put_color(_mh_term_t *terminal, uint8_t r, uint8_t g, uint8_t b, uint8_t x, uint8_t y) {
	if (x < 0 || x >= terminal->width || y < 0 || y >= terminal->height) {
		return;
	}

	_mh_color_t *color = &terminal->screen[x + y * terminal->width].color;
	color->r = r;
	color->g = g;
	color->b = b;
}

void terminal_put_string(_mh_term_t *terminal, const char *str, mh_bounds_t bounds, uint8_t r, uint8_t g, uint8_t b) {
	uint8_t x = (uint8_t)((bounds & 0xff000000) >> 0x18);
	uint8_t y = (uint8_t)((bounds & 0x00ff0000) >> 0x10);
	uint8_t w = (uint8_t)((bounds & 0x0000ff00) >> 0x08);
	uint8_t h = (uint8_t)(bounds & 0x000000ff);

	if (w <= x || h <= y) {
		return;
	}

	uint8_t y_offset = 0;
	uint8_t x_offset = 0;
	for (uint8_t i = 0; i < strlen(str); i++) {
		if (x_offset >= w) {
			y_offset++;
			x_offset = 0;
		}

		if (y_offset >= h) {
			break;
		}

		terminal_put_color(terminal, r, g, b, x + x_offset, y + y_offset);
		terminal_put_char(terminal, str[i], x + x_offset, y + y_offset);

		x_offset++;
	}
}

void terminal_put_string_f(_mh_term_t *terminal, const char *str, mh_bounds_t bounds, uint8_t r, uint8_t g, uint8_t b, ...) {
	char *buf = (char*)malloc(sizeof(char) * 256);
	size_t buf_size = 256;

	va_list args;
	va_start(args, b);
	uint32_t length = vsnprintf(buf, buf_size, str, args);
	va_end(args);

	if (length + 1 > buf_size) {
		buf_size = length + 1;
		buf = (char*)realloc(buf, buf_size);

		va_start(args, b);
		vsnprintf(buf, buf_size, str, args);
		va_end(args);
	}

	terminal_put_string(terminal, buf, bounds, r, g, b);
	free(buf);
}

void terminal_get_key(_mh_term_t *terminal, _mh_key_t *key) {
	SDL_Event e;
	_mh_key_t query = KEY_UNKNOWN;

	while (SDL_PollEvent(&e)) {
		switch (e.type) {
		case SDL_QUIT:
			terminal->shutdown = true;
			break;

		case SDL_KEYDOWN:;
			query = _sdl_scancode_to_mh_key_t[e.key.keysym.scancode];
			if (query != KEY_UNKNOWN && !terminal->keys_down[query]) {
				*key = query;
				terminal->keys_down[query] = true;
			}
			break;

		case SDL_KEYUP:;
			query = _sdl_scancode_to_mh_key_t[e.key.keysym.scancode];
			if (query != KEY_UNKNOWN && terminal->keys_down[query]) {
				terminal->keys_down[query] = false;
			}
			break;
		}
	}
}
